#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/types.h>
#define MAX 1000000

typedef struct TNode{
    char info[MAX];
	char new_path[MAX];
    struct TNode* next;
}TNode;

TNode *init=NULL;

int Insert(const char* info, const char* new_path);
int RemoveLast();
DIR * Ler_diretorio(const char* path);
char* delete_format(const char* file_name);
char* join_path(const char* path1, const char* path2);
void tar(const char* path, char* target);
void producer(const char* path, const char* new_path);
void consumer(const char* info, const char* path);
void copy_file(const char* ori, const char* target);

int main(int argc, char **argv){
	TNode *aux;
	pid_t process;
	FILE *command;
	char t_folder[MAX], command_tar[MAX], rm_t_folder[MAX];
	int status=-1;

	if(argv[1][strlen(argv[1])-1] == '/')
		argv[1][strlen(argv[1])-1] = '\0';

	char new_path[MAX];
	strncpy(new_path, argv[2], strlen(argv[2])-4);
	//printf("%s\n", new_path);
	tar(argv[1], new_path);
	aux = init;
	//printf("********\n");
	
	int count = 0;

	while(init != NULL){
		if(count <= 4){
			count++;
			aux = init;
			while(aux->next != NULL){
				aux = aux->next;
			}
			process = fork();
			if(!process){
				//printf("Consuming!! \n");
				consumer(aux->info, aux->new_path);
				exit(0);
			}
			else{
				//printf("REMOVE !!\n");
				RemoveLast();
			}
		}
		else{
			wait(&status);
			count--;
		}
	}
	pid_t temp = wait(&status);
	while(temp != -1){
		temp = wait(&status);
		//printf("%d\n", temp);
	}

	//printf("%d\n", status);
	strcpy(t_folder, new_path);
	strcat(command_tar, "tar -cf ");
	strcat(command_tar, t_folder);
	strcat(command_tar, ".tar ");
	strcat(command_tar, t_folder);
	strcat(command_tar, "/");
	command = popen(command_tar, "r");
	pclose(command);
//	strcat(rm_t_folder, "rm -r ");
//	strcat(rm_t_folder, t_folder);
//	strcat(rm_t_folder,"/");
//
//	command = popen(rm_t_folder, "r");
//	pclose(command);


	return 0;

}

int Insert(const char* info, const char* new_path){
    TNode *p;
    p = (TNode*) malloc(sizeof(TNode));
    if(p == NULL){
        return 0;
    }

    strcpy(p->info, info);
	strcpy(p->new_path, new_path);
    p->next = NULL;


    if (init == NULL){
        init = p;
    }
    else{
        p->next = init;
        init = p;
    }
    return 1;
}

int RemoveLast(){
    TNode *ptr = init;
    TNode *temp = init;

    if(init == NULL){
        return 0;
    }
    else{
		if(ptr->next == NULL){
			init = NULL;
		}else{
			while(ptr->next != NULL){
					temp = ptr;
					ptr = ptr->next;
			}
			temp->next = NULL;
		}
        free(ptr);
    }
    return 1;
}

void producer(const char* path, const char* new_path){
	int sucess = Insert(path, new_path);
	if(!sucess){
		//printf("Impossivel inserir na fila !!!\n");
	}
	return;
}

void consumer(const char* info, const char* path){
	FILE *command;
	char zip[MAX];
	strcat(zip, "bzip2 ");
	strcat(zip, path);
	strcat(zip, " 2>out");
	command = popen(zip, "r");
	pclose(command);
	zip[0] = '\0';
}

void copy_file(const char* ori, const char* target){
	FILE *f1, *f2;
	unsigned char c[4096];
	f1 = fopen(ori, "rb");
	f2 = fopen(target, "wb");
	size_t len;

	while((len = fread(c, sizeof(unsigned char), 4096, f1))){
		fwrite(c, sizeof(unsigned char), len, f2);
	}

	fclose(f1);
	fclose(f2);

}

char* delete_format(const char* file_name){
	char* new_name = (char*) malloc((strlen(file_name)+5)*sizeof(char));
	char p = '.';
	for(int i=0; i < strlen(file_name); i++){
		if(file_name[i] == p){
			break;
		}
		new_name[i] = file_name[i];
	}
	strcat(new_name, ".bz2");
return new_name;
}

char* join_path(const char* path1, const char* path2){
	char* new_path;
	new_path =(char*)malloc((strlen(path1)+strlen(path2))+2);
	strcpy(new_path, path1);
	strcat(new_path, "/");
	strcat(new_path, path2);

	return new_path;

}

DIR * Ler_diretorio(const char* path){
	DIR* diretorio = opendir(path);
	if(diretorio == NULL){
		//printf("Impossivel abrir diretorio ou diretorio nao existente !!!");
		return 0;
	}

	return diretorio;
}

void tar(const char* path, char* t_folder){

	DIR *dir;
	FILE *command;
	struct dirent *listdir;
	//const char* new_name;
	const char* folder_name;
	const char* t_name;

	if(t_folder[strlen(t_folder)-1] == '/')
		t_folder[strlen(t_folder)-1] = '\0';

	struct stat fstat;
	mkdir(t_folder, 0777);

	dir = Ler_diretorio(path);

	while((listdir = readdir(dir)) != NULL){

		if(strcmp(listdir->d_name, ".") == 0 || strcmp(listdir->d_name, "..") == 0){
			continue;
		}

		folder_name = join_path(path, listdir->d_name);
		t_name = join_path(t_folder, listdir->d_name);

		if(stat(folder_name, &fstat) < 0)
        	continue;
        if(S_ISDIR(fstat.st_mode))
        {
			char target[MAX];
			strcpy(target, t_name);
			tar(folder_name, target);
        }
	}

	dir = Ler_diretorio(path);

	while((listdir = readdir(dir)) != NULL){

		if(strcmp(listdir->d_name, ".") == 0 || strcmp(listdir->d_name, "..") == 0){
			continue;
		}

		folder_name = join_path(path, listdir->d_name);
		t_name = join_path(t_folder, listdir->d_name);
		if(stat(folder_name, &fstat) < 0)
        	continue;
        if(!S_ISDIR(fstat.st_mode))
        {
			copy_file(folder_name, t_name);
			producer(folder_name, t_name);
        }
	}

	closedir(dir);

	return;

}
