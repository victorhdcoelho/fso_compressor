import bs4
import requests
from tqdm import tqdm
import sys

# start_url = "https://en.wikipedia.org/wiki/Special:Random"
start_url = "https://pt.wikipedia.org/wiki/Special:Random"
target_url = "https://en.wikipedia.org/wiki/Narendra_Modi"


def find_first(url):
    response = requests.get(url)
    html = response.text
    soup = bs4.BeautifulSoup(html, "html.parser")
    return soup


if __name__ == "__main__":
    for i in tqdm(range(int(sys.argv[1]))):
        text = find_first(start_url)
        string = text.title.string
        string = string.replace(" ", "")
        string = string.replace("(", "")
        string = string.replace(")", "")
        string = string.replace(",", "")
        string = string.replace(".", "")
        string = string.replace("'", "")
        f = open("ORI/{}.txt".format(string), "w")
        f.write(text.p.get_text())
        f.close()
